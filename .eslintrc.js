module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended'],
  rules: {
    'no-console': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'generator-star-spacing': 'off',
    'no-mixed-operators': 0,
    // should be on a new line
    'vue/max-attributes-per-line': [
      0,
      {
        'singleline': 5,
        'multiline': {
          'max': 1,
          'allowFirstLine': false
        }
      }
    ],
    'vue/attribute-hyphenation': 0,
    'vue/html-self-closing': 0,
    'vue/component-name-in-template-casing': 0,
    'vue/html-closing-bracket-spacing': 0,
    'vue/singleline-html-element-content-newline': 0,
    'vue/no-unused-components': 0,
    'vue/multiline-html-element-content-newline': 0,
    'vue/no-use-v-if-with-v-for': 0,
    'vue/html-closing-bracket-newline': 0,
    'vue/no-parsing-error': 0,
    "vue/name-property-casing": ["error", "PascalCase"],
    "vue/no-v-html": "off",
    'no-tabs': 0,
    'accessor-pairs': 2,
    'quotes': [
      2,
      'single',
      {
        'avoidEscape': true,
        'allowTemplateLiterals': true
      }
    ],
    'semi': [
      2,
      'never',
      {
        'beforeStatementContinuationChars': 'never'
      }
    ],
    'no-delete-var': 2,
    'prefer-const': [
      2,
      {
        'ignoreReadBeforeAssign': false
      }
    ],
    "vue/html-indent": 'off',
    "vue/no-multi-spaces": ["error", {
      "ignoreProperties": false
    }],
    'template-curly-spacing': 'off',
    'indent': 0,
    //空行最多不能超过100行
    "no-multiple-empty-lines": [0, { "max": 100 }],
    //关闭禁止混用tab和空格
    "no-mixed-spaces-and-tabs": 'off',
    //是否允许对象中出现结尾逗号
    "comma-dangle": 'off',
    //一行结束后面不要有空格
    "no-trailing-spaces": 'off',
    //不能用多余的空格
    "no-multi-spaces": 0,
    //不能有声明后未被使用的变量或参数
    "no-unused-vars": 'off',
    //函数定义时括号前面要不要有空格
    "space-before-function-paren": 'off',
    //注释风格要不要有空格什么的
    "spaced-comment": 0,
    //不能有不规则的空格
    "no-irregular-whitespace": 0,
    //配置camelcase选项
    'camelcase': [0, { properties: 'always' }],
    //换行风格
    'linebreak-style': [0, 'error', 'windows']
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
