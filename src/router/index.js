import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */
/**
 * 
 * 说明   
 * 
 * 1、meta: { affix: true } tagsview固定钉显示
 * 
 * 2、meta: { noCache: true }  针对动态编辑meta.title的页面添加noCache 保证 tagsview与当前页面title一致
 */
/* Layout */
import Layout from '@/layout'

// 基础路由
export const constantRoutes = [
  /**
 * 解决tagsview当前页面刷新问题
 */
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index'),
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/302',
    component: () => import('@/views/302'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
    }]
  },

]
/**
 * 动态路由
 */
export const asyncRoutes = [
  {
    path: '/rolemanage',
    component: Layout,
    redirect: '/rolemanage/index',
    meta: { noCache: true, roles: ['admin'] },
    children: [{
      path: 'index',
      name: 'RoleManage',
      component: () => import('@/views/rolemanage/index'),
      meta: { noCache: true, title: '角色管理', icon: 'el-icon-s-data' }
    }, {
      path: 'editrole/index',
      name: 'EditRole',
      hidden: true,
      component: () => import('@/views/rolemanage/editrole/index'),
      meta: { noCache: true, activeMenu: '/rolemanage/index', title: '编辑角色' }
    }]
  },
  {
    path: '/accountmanage',
    component: Layout,
    redirect: '/accountmanage/index',
    meta: { noCache: true, roles: ['admin'] },
    children: [{
      path: 'index',
      name: 'AccountManage',
      component: () => import('@/views/accountmanage/index'),
      meta: { noCache: true, title: '账号管理', icon: 'el-icon-s-data' }
    }]
  },
  {
    path: '/financemanage',
    component: Layout,
    redirect: '/financemanage/supplierrevenuedetail/index',
    meta: { noCache: true, title: '财务管理', icon: 'el-icon-s-promotion', roles: ['admin'] },
    children: [{
      path: 'supplierrevenuedetail/index',
      name: 'SupplieRevenueDetail',
      component: () => import('@/views/financemanage/supplierrevenuedetail/index'),
      meta: { noCache: true, title: '供应商收益明细', icon: 'el-icon-s-flag' }
    }, {
      path: 'supplierwithdrawal/index',
      name: 'SupplierWithdrawal',
      component: () => import('@/views/financemanage/supplierwithdrawal/index'),
      meta: { noCache: true, title: '供应商提现管理', icon: 'el-icon-s-management' }
    }, {
      path: 'merchantsrecharge/index',
      name: 'MerchantsRecharge',
      component: () => import('@/views/financemanage/merchantsrecharge/index'),
      meta: { noCache: true, title: '商家充值管理', icon: 'el-icon-s-ticket' }
    }]
  },
  {
    path: '/systemmanage',
    component: Layout,
    redirect: '/systemmanage/ratiosetting/index',
    alwaysShow: true,
    meta: { noCache: true, title: '系统设置', icon: 'el-icon-s-promotion', roles: ['admin'] },
    children: [{
      path: 'ratiosetting/index',
      name: 'RatioSetting',
      component: () => import('@/views/systemmanage/ratiosetting/index'),
      meta: { noCache: true, title: '统一供货价比例设置', icon: 'el-icon-s-data' }
    }, {
      path: 'updatepassword/index',
      name: 'UpdatePassword',
      hidden: true,
      component: () => import('@/views/systemmanage/updatePassword/index'),
      meta: { noCache: true, title: '修改密码' }
    },]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }

]
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
