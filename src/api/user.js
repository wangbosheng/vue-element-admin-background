import request from '@/utils/request'
// import qs from 'qs';
const Api = {
  LoginIn: '/user/login',
  StudentInfo: '/user/info',
}


export function login(parameter) {
  return request({
    baseURL: 'https://www.fastmock.site/mock/f6273fce31c98c4d64fd82d91784712f/api',
    url: Api.LoginIn,
    method: 'get',
    params: parameter
  })
}

export function getInfo(token) {
  return request({
    baseURL: 'https://www.fastmock.site/mock/f6273fce31c98c4d64fd82d91784712f/api',
    url: Api.StudentInfo,
    method: 'get',
  })
}